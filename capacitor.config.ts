import { CapacitorConfig } from '@capacitor/cli';
import { config as dotEnvConfig } from 'dotenv';

dotEnvConfig(); // Read .env file for specific configs

const { PROJECT, ANDROID_ID, IOS_ID, PLATFORM } = process.env;

const config: CapacitorConfig = {
  appId: PLATFORM === 'android' ? ANDROID_ID : IOS_ID,
  appName: PROJECT,
  webDir: `apps/${PROJECT}/build`,
  bundledWebRuntime: false,
  android: {
    path: `apps/${PROJECT}/android`,
  },
  ios: {
    path: `apps/${PROJECT}/ios`,
  },
  plugins: {
    PushNotifications: {
      presentationOptions: ['badge', 'sound', 'alert'],
    },
  },
};

export default config;
