const { resolve } = require('path');

const { merge } = require('webpack-merge');

module.exports = {
  webpack: {
    configure: (webpackConfig, { env, paths }) => {
      return merge(webpackConfig, {
        resolve: {
          modules: ['node_modules', resolve(__dirname, 'src')],
          extensions: ['.js', '.jsx'],
        },
        module: {
          rules: [
            {
              test: /\.(svg|gif|jpe?g|tiff?|png|webp|bmp)$/i,
              loader: 'file-replace-loader',
              options: {
                condition: 'if-replacement-exists',
                replacement(resourcePath) {
                  if (process.env.PROJECT !== 'toon') {
                    return resourcePath.replace(/\/img\/toon\//, `/img/${process.env.PROJECT}/`);
                  }
                  return resourcePath;
                },
                async: true,
              },
            },
          ],
        },
      });
    },
  },
  babel: {
    plugins: ['@babel/plugin-proposal-export-default-from'],
  },
};
